<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    ['prefix' => 'covid19-tracking', 'as' => 'covid19.'],
    function () {
        Route::post('register', 'RegisterApiController@register')->name('auth.register');

        Route::group(
            ['middleware' => 'auth:api'],
            function () {
                Route::post('location', 'LocationApiController@create')->name('location.create');
                Route::post('update-covid-positive-status', 'UserApiController@updateCovidPositiveFlag')->name('user.update-covid-positive-flag');
                Route::get('interaction', 'InteractionApiController@getInteractionList')->name('interaction.list');
                Route::get('user/profile', 'UserApiController@getUserProfile')->name('user.profile.get');
            }
        );
});
