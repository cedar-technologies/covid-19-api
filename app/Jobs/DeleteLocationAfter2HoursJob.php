<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Location;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DeleteLocationAfter2HoursJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            DB::transaction(function () {
                Location::where('timestamps', '<', Carbon::now()->subHours(2))
                    ->delete();
            });
            
            echo "Delete location success.\n";

        } catch (\Exception $e) {
            \Log::error($e);
            echo $e->getMessage() . "\n";
        }
    }
}
