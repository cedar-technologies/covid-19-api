<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\DeleteLocationAfter2HoursJob;

class DeleteLocationAfter2Hours extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:location-after-2-hours';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete location data after 2 hours to save space.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new DeleteLocationAfter2HoursJob());
    }
}
