<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GetInteractionRequest;
use App\Services\InteractionService;

class InteractionApiController extends ApiController
{
    public function __construct()
    {
        $this->interaction_service = new InteractionService;
    }

    public function getInteractionList(GetInteractionRequest $request)
    {
        $response = $this->interaction_service->getInteractionList($request);

        return $this->formatGeneralResponse(
            $response['message'],
            $response['http_code'],
            $response['attributes']
        );
    }
}
