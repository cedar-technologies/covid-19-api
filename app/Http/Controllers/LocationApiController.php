<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LocationRequest;
use App\Services\LocationService;

class LocationApiController extends ApiController
{
    public function __construct()
    {
        $this->location_service = new LocationService;
    }

    public function create(LocationRequest $request)
    {
        $response = $this->location_service->create($request);

        return $this->formatGeneralResponse(
            $response['message'],
            $response['http_code'],
            $response['attributes'] ?? null
        );
    }
}
