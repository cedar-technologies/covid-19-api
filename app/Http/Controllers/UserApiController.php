<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserUpdateCovidPositiveRequest;
use App\Http\Requests\UserGetProfileRequest;
use App\Services\UserService;

class UserApiController extends ApiController
{
    public function __construct()
    {
        $this->user_service = new UserService;
    }

    public function updateCovidPositiveFlag(UserUpdateCovidPositiveRequest $request)
    {
        $response = $this->user_service->updateCovidPositiveFlag($request);

        return $this->formatGeneralResponse(
            $response['message'],
            $response['http_code']
        );
    }

    public function getUserProfile(UserGetProfileRequest $request)
    {
        $response = $this->user_service->getUserProfile($request);

        return $this->formatGeneralResponse(
            $response['message'],
            $response['http_code'],
            $response['attributes'] ?? null
        );
    }
    
}
