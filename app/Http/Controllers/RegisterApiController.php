<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Services\AuthService;

class RegisterApiController extends ApiController
{
    public function __construct()
    {
        $this->auth_service = new AuthService;
    }

    public function register(RegisterRequest $request)
    {
        $response = $this->auth_service->register($request);

        return $this->formatGeneralResponse(
            $response['message'],
            $response['http_code'],
            $response['attributes']
        );
    }
}
