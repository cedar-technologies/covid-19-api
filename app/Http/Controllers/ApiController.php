<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs;

    public function formatGeneralResponse($message, $http_code, $attributes = [])
    {
        $response = [
            'message' => $message
        ];

        if (!empty($attributes)) {
            $response['attributes'] = $attributes;
        }

        return response()->json($response, $http_code);
    }
}
