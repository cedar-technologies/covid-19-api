<?php

namespace App\Services;

use App\Models\User;
use App\Models\Location;
use App\Models\Interaction;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class LocationService extends BaseService
{
    public function create($request)
    {
        try {
            $user  = User::select('id', 'notify')
                ->whereMobileNo($request->mobile_no)
                ->first();

            if (!$user) {
                return $this->formatGeneralResponse(
                    'Mobile number is not registered. Please register first.',
                    404
                );
            }

            $close_distance_records = Location::where('sublocation', $request->sublocation)
                ->where('timestamps', $request->timestamps)
                ->computeCloseContact($request->latitude, $request->longitude, config('staticdata.distance_threshold'))
                ->get();
            
            DB::transaction(function () use ($request, $close_distance_records, $user, &$notify){
                foreach ($close_distance_records as $record) {
                    $check_exists = Interaction::where('interaction_ended_at', Carbon::parse($request->timestamps)->addSeconds(config('staticdata.store_location_interval')))
                        ->where(
                            function ($q) use ($request) {
                                $q->where('mobile_no_1', $request->mobile_no)
                                    ->orWhere('mobile_no_2', $request->mobile_no);
                            }
                        )->where(
                            function ($q) use ($record) {
                                $q->where('mobile_no_1', $record->mobile_no)
                                    ->orWhere('mobile_no_2', $record->mobile_no);
                            }
                        )->exists();

                    $previous_interaction = Interaction::where('interaction_ended_at', $request->timestamps)
                        ->where(
                            function ($q) use ($request) {
                                $q->where('mobile_no_1', $request->mobile_no)
                                    ->orWhere('mobile_no_2', $request->mobile_no);
                            }
                        )->where(
                            function ($q) use ($record) {
                                $q->where('mobile_no_1', $record->mobile_no)
                                    ->orWhere('mobile_no_2', $record->mobile_no);
                            }
                        )->first();

                    if (!$check_exists && !$previous_interaction && $request->mobile_no != $record->mobile_no) {
                        $interaction_history_of_two_numbers = Interaction::select('mobile_no_1', 'mobile_no_2')
                            ->where(
                                function ($q) use ($request) {
                                    $q->where('mobile_no_1', $request->mobile_no)
                                        ->orWhere('mobile_no_2', $request->mobile_no);
                                }
                            )->where(
                                function ($q) use ($record) {
                                    $q->where('mobile_no_1', $record->mobile_no)
                                        ->orWhere('mobile_no_2', $record->mobile_no);
                                }
                            )->first();

                        if ($interaction_history_of_two_numbers) {
                            $mobile_no_1 = $interaction_history_of_two_numbers->mobile_no_1;
                            $mobile_no_2 = $interaction_history_of_two_numbers->mobile_no_2;
                        } else {
                            $mobile_no_1 = $request->mobile_no;
                            $mobile_no_2 = $record->mobile_no;
                        }
                        
                        Interaction::create(
                            [
                                'mobile_no_1' => $mobile_no_1,
                                'mobile_no_2' => $mobile_no_2,
                                'latitude' => $request->latitude,
                                'longitude' => $request->longitude,
                                'distance' => $record->distance,
                                'min_distance' => $record->distance,
                                'max_distance' => $record->distance,
                                'interaction_started_at' => $request->timestamps,
                                'interaction_ended_at' => Carbon::parse($request->timestamps)->addSeconds(config('staticdata.store_location_interval')),
                            ]
                        );
                    } else if (!$check_exists && $previous_interaction && $request->mobile_no != $record->mobile_no) {
                        $started_at = Carbon::parse($previous_interaction->interaction_started_at);
                        $ended_at = Carbon::parse($previous_interaction->interaction_ended_at);
                        $duration = $started_at->diffInSeconds($ended_at);
                        $total_records = $duration / config('staticdata.store_location_interval');
                        $average_distance = ($previous_interaction->distance * $total_records + $record->distance) / ($total_records + 1);
                        $min_distance = $previous_interaction->min_distance < $record->distance ? $previous_interaction->min_distance : $record->distance;
                        $max_distance = $previous_interaction->max_distance > $record->distance ? $previous_interaction->max_distance : $record->distance;
                        $previous_interaction->update(
                            [
                                'interaction_ended_at' => Carbon::parse($previous_interaction->interaction_ended_at)->addSeconds(config('staticdata.store_location_interval')),
                                'distance' => $average_distance,
                                'min_distance' => $min_distance,
                                'max_distance' => $max_distance
                            ]
                        );
                    }
                }
                
                Location::create($request->all());
                
                if ($user->notify == 1) {
                    $notify = true;
                    $user->update(['notify' => false]);
                } else {
                    $notify = false;
                }

            });

            return $this->formatGeneralResponse(
                'Location has been stored and interaction computation has been executed and stored if there is any close contact.',
                200,
                ['notify' => $notify]
            );
        } catch (\Exception $e) {
            \Log::error($e);
            return $this->formatGeneralResponse(
                'Failed to store location and compute interaction',
                500,
                ['errors' => $e->getMessage()]
            );
        }
    }
}