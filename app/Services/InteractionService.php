<?php

namespace App\Services;

use App\Models\Interaction;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class InteractionService extends BaseService
{
    public function getInteractionList($request)
    {
        try {
            $request->mobile_no = '+' . $request->mobile_no;
            
            $interactions = Interaction::where(
                    function ($q) use ($request) {
                        return $q->where('mobile_no_1', $request->mobile_no)
                            ->orWhere('mobile_no_2', $request->mobile_no);
                    }
                )->when(
                    $request->duration,
                    function($q) use ($request) {
                        return $q->where('interaction_started_at', '>=', Carbon::now()->subDays($request->duration))
                            ->where('interaction_ended_at', '<=', Carbon::now());
                    }
                )->get();

            return $this->formatGeneralResponse(
                'Get user interaction success.',
                200,
                ['interactions' => $interactions]
            );
        } catch (\Exception $e) {
            \Log::error($e);
            return $this->formatGeneralResponse(
                'Failed to get user interaction.',
                500,
                ['errors' => $e->getMessage()]
            );
        }
    }
}