<?php

namespace App\Services;

class BaseService
{
    public function formatGeneralResponse($message, $http_code, $attributes = [])
    {
        $response = [
            'http_code' => $http_code,
            'message' => $message
        ];

        if (!empty($attributes)) {
            $response['attributes'] = $attributes;
        }

        return $response;
    }
}
