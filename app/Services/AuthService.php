<?php

namespace App\Services;

use App\Models\User;

class AuthService extends BaseService
{
    public function register($request)
    {
        try {
            $user = User::whereMobileNo($request->mobile_no)->first();

            if (!$user) {
                $user = User::create(['mobile_no' => $request->mobile_no]);
            }
            
            $token = $user->createToken('user token')->accessToken;

            return $this->formatGeneralResponse(
                'User mobile number has been registered.',
                200,
                ['token' => $token]
            );
        } catch (\Exception $e) {
            \Log::error($e);
            return $this->formatGeneralResponse(
                'Failed to register',
                500,
                ['errors' => $e->getMessage()]
            );
        }
    }
}