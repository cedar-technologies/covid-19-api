<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserService extends BaseService
{
    public function updateCovidPositiveFlag($request)
    {
        try {
            $user = User::whereMobileNo($request->mobile_no)->first();
            
            if (!$user) {
                return $this->formatGeneralResponse(
                    'No user found with given mobile number',
                    404
                );
            }

            $user->update(
                [
                    'is_covid_positive' => $request->is_covid_positive,
                    'declared_covid_positive_at' => Carbon::now()
                ]
            );
            
            $status = $request->is_covid_positive ? 'positive.' : 'negative.';
            
            return $this->formatGeneralResponse(
                'User covid-19 status has been changed. Current status: ' . $status,
                200
            );
        } catch (\Exception $e) {
            \Log::error($e);
            return $this->formatGeneralResponse(
                'Failed to update user covid-19 status',
                500,
                ['errors' => $e->getMessage()]
            );
        }
    }

    public function getUserProfile($request) {
        try {
            $request->mobile_no = '+' . $request->mobile_no;
            
            $user = User::whereMobileNo($request->mobile_no)->first();

            if (!$user) {
                return $this->formatGeneralResponse(
                    'No user found with given mobile number',
                    404
                );
            }

            return $this->formatGeneralResponse(
                'Get user profile success.',
                200,
                ['user_profile' => $user]
            );
        } catch (\Exception $e) {
            \Log::error($e);
            return $this->formatGeneralResponse(
                'Failed to get user profile.',
                500,
                ['errors' => $e->getMessage()]
            );
        }
    }
}