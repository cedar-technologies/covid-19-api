<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    
    protected $fillable = [
        'mobile_no',
        'is_doctor',
        'is_government',
        'is_covid_positive',
        'declared_covid_positive_at',
        'notify'
    ];
}
