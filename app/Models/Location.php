<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'mobile_no',
        'longitude',
        'latitude',
        'sublocation',
        'timestamps',
    ];

    public function scopeComputeCloseContact($query, $latitude, $longitude, $radius = 15){
        $haversine = '( 6371000 * acos( cos( radians('.$latitude.') ) *
                 cos( radians( latitude ) )
                 * cos( radians( longitude ) - radians('.$longitude.')
                 ) + sin( radians('.$latitude.') ) *
                 sin( radians( latitude ) ) )
               )';

        return $query->select('mobile_no')
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$radius]);
    }
}
