<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Interaction extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'mobile_no_1',
        'mobile_no_2',
        'longitude',
        'latitude',
        'distance',
        'min_distance',
        'max_distance',
        'interaction_started_at',
        'interaction_ended_at'
    ];
}
