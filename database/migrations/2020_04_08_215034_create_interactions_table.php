<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInteractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mobile_no_1', 20)->index();
            $table->string('mobile_no_2', 20)->index();
            $table->decimal('latitude', 20, 17);
            $table->decimal('longitude', 20, 17);
            $table->decimal('distance', 20, 17);
            $table->datetime('timestamps')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interactions');
    }
}
