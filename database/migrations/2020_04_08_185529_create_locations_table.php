<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mobile_no', 20);
            $table->decimal('latitude', 20, 17);
            $table->decimal('longitude', 20, 17);
            $table->string('sublocation', 30);
            $table->datetime('timestamps')->index();
            $table->index(['mobile_no', 'sublocation']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
