<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInteractionEndedAtAndRenameTimestampsToInteractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('interactions', function (Blueprint $table) {
            $table->renameColumn('timestamps', 'interaction_started_at');
            $table->datetime('interaction_ended_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('interactions', function (Blueprint $table) {
            $table->renameColumn('interaction_started_at', 'timestamps');
            $table->dropColumn('interaction_ended_at');
        });
    }
}
