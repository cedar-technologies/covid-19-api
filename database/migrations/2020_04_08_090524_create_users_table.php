<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mobile_no', 20)->unique();
            $table->boolean('is_admin')->default(false);
            $table->boolean('is_government')->default(false);
            $table->boolean('is_covid_positive')->default(false);
            $table->datetime('declared_covid_positive_at')->nullable();
            $table->text('token')->nullable();
            $table->index(['mobile_no', 'is_covid_positive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
