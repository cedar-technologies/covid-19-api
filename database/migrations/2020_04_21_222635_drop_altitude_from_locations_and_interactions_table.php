<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAltitudeFromLocationsAndInteractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->dropColumn('altitude');
        });

        Schema::table('interactions', function (Blueprint $table) {
            $table->dropColumn('altitude');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->decimal('altitude', 20, 17);
        });

        Schema::table('interactions', function (Blueprint $table) {
            $table->decimal('altitude', 20, 17);
        });
    }
}
